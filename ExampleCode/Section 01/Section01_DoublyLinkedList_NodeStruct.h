/*************************************
 * File name: Section01_DoublyLinkedList_NodeStruct.h 
 * Author: Matthew Morrison
 * Email: matt.morrison@nd.edu 
 * 
 * Contains class methods for a Doubly Linked List 
 * ***********************************/

#ifndef SECTION01_DOUBLYLINKEDLIST_NODESTRUCT_H
#define SECTION01_DOUBLYLINKEDLIST_NODESTRUCT_H

#include "Section01_DLLNodeStruct.h"

template<class T>
class DLList{
    
    public:
    
        Node<T> *head;
        Node<T> *tail;
        
        /************************************
         * Function Name: LinkedList<T>
         * Preconditions: none
         * Postconditions: none 
         * Constructor with a new Node 
         * *********************************/
        DLList<T>()
        {
            // new Node() creates a Node will a nullptr
            this->head = new Node<T>();
        }

        /************************************ 
         * Function Name: ~LinkedList<T>
         * Preconditions: none
         * Postconditions: none 
         * Destructor 
         * *********************************/
        ~DLList<T>()
        {
            Node<T>* current = head;            // (1)
        
            while (current != nullptr)
            { 
                Node<T>* next = current->next;  // (2)
                delete current;                 // (3)
                current = next;                 // (4)
            }
        
            head = nullptr;
        }

        /************************************
         * Function Name: operator=
         * Preconditions: const LinkedList<T>&
         * Postconditions: none 
         * Assignment operator  
         * *********************************/		
		DLList<T>& operator=(const DLList<T>& copy) {
		    Node<T>* temp = copy.head;
			while(temp != nullptr){
			    this->insert(temp->data);   // (1)
			    temp = temp->next;          // (2)
			}
		}

        /************************************
         * Function Name: insert()
         * Preconditions: T 
         * Postconditions: none
         * Inserts the new element at the end of the linked list   
         * *********************************/          
        void insert(T value){
            if (head->next == NULL)
            {
                //head = new Node<T>();
                tail = head;                    // Case 1: (1)
                head->next = tail;              // Case 1: (2)
                head->data = value;             // Case 1: (3)
            }
            else
            {
                tail->next = new Node<T>();     // Case 2: (1)
                tail->next->prev = tail;        // Case 2: (2)
                tail = tail->next;              // Case 2: (3)
                tail->data = value;             // Case 2: (4)
                tail->next = nullptr;           // Case 2: (5)
            }   
            length++;
        }

       /************************************
         * Function Name: setNodeTail
         * Preconditions: Node<T>*
         * Postconditions: none
         * Sets a new Node tail  
         * *********************************/          
        void setNodeTail(Node<T>* temp){
            tail = temp;
        }

       /************************************
         * Function Name: removeNode
         * Preconditions: Node<T>*
         * Postconditions: none
         * Sets a new Node tail  
         * *********************************/         
        void removeNode(Node<T>* node)
        {
            if(node == nullptr)
                throw std::out_of_range("invalid LinkedList Node");
            
            else if(node->next != nullptr){
                node->next = node->next->next;
                length--;
            }
        }
       /************************************
         * Function Name: deleteNode
         * Preconditions: T
         * Postconditions: none
         * Deletes a node with the data T key 
         * *********************************/   
        void deleteNode(T key){
            
            if(head == nullptr)
                throw std::out_of_range("invalid LinkedList Node");
            
            else if(head->data == key){         // Case 2
                head = head->next;              // Case 2 : (1)
                if(head != nullptr)             // Case 2 : (2)
                    head->prev = nullptr;   
                length--;
                return;
                
            }
            
            Node<T>* current = head;
            Node<T>* previous = nullptr;
            
            while(current != nullptr && current->data != key){
                previous = current;
                current = current->next;
            }
            
            //delete cur node
            previous->next = current->next;         // Case 3: (1)
            if(previous->next != nullptr){
                previous->next->prev = previous;    // Case 3: (2)
            }
            if(current == tail){
                tail = nullptr;
                previous->next = tail;              // Case 3: (3)
                tail = previous;                    // Case 4: (4)
            }
            delete current;
            length--;
        }
		
       /************************************
         * Function Name: getLength
         * Preconditions: none 
         * Postconditions: size_t
         * Returns the length of the array  
         * *********************************/           
        size_t getLength(){
            return length;
        }
        
        /************************************
         * Function Name: createCycle()
         * Preconditions: none
         * Postconditions: none
         * Sets the tail of new List equal to the head  
         * *********************************/          
        void createCycle(){
            tail->next = head;      // (1)
            head->prev = tail;      // (2)
        }

       /************************************
         * Function Name: breakCycle()
         * Preconditions: none
         * Postconditions: none
         * Sets the tail of new List equal to a nullptr
         * must only be called after a createCycle call 
         * *********************************/          
        void breakCycle(){
            tail->next = nullptr;
            head->prev = nullptr;
        }
        
        friend std::ostream& operator<< (std::ostream& stream, const DLList<T>* theList){
            Node<T>* temp;

            if (theList->head == NULL)
            {
                return stream;
            }
        
            temp = theList->head;
        
            while (temp != NULL)
            {
                stream << temp->data << " " ;
                temp = temp->next;
            }
            
            return stream;
        }
    
    private:
        size_t length = 0;
    
};

/************************************
 * Function Name: rotateRight()
 * Preconditions: int
 * Postconditions: none
 * creates a cycle, then rotates the head and 
 * tail by the number represented by the integer 
 * then breaks the cycle 
 * *********************************/ 
template<class T>
void rotateRight(DLList<T>* list, int rotations){
    
    // Create the cycle 
    list->createCycle();

    // Move the pointer to the right by one each loop
    while(rotations > 0){
        list->head = list->head->prev;
        list->tail = list->tail->prev;
        rotations--;
    }
    
    // break the cycle 
    list->breakCycle();
}

/************************************
 * Function Name: rotateLeft()
 * Preconditions: int
 * Postconditions: none
 * creates a cycle, then rotates the head and 
 * tail by the number represented by the integer 
 * then breaks the cycle 
 * *********************************/ 
template<class T>
void rotateLeft(DLList<T>* list, int rotations){
    
    // Create the cycle 
    list->createCycle();

    // Move the pointer to the right by one each loop
    while(rotations > 0){
        list->head = list->head->next;      // 1 
        list->tail = list->tail->next;      // 2 
        rotations--;
    }
    
    // break the cycle 
    list->breakCycle();
}

#endif