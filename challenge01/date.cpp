//implentation of testDate
/*************************************
 * * File name: date.cpp
 * * Author: Stephen Grisoli Ntahaniel Ekwueme Aidan Gonzales Michael Deranek
 * * Email: sgrisoli@nd.edu nekwueme@nd.edu agonza31@nd.edu sgrisoli@nd.edu
 * * Implentation of Date class
 * * Note: any references or citations concerning outside
 * * ***********************************/

#include <iostream>
#include "date.h"
#include <vector>
#include <string>
#include <fstream>
using namespace std;

/************************************************
 * * Function Name: Date
 * * Preconditions: none
 * * Postconditions: date
 * * Default constructor for Date class
 ************************************************/
Date::Date()
{
}

/************************************************
 * * Function Name: Date
 * * Preconditions: int,int,int
 * * Postconditions: date
 * * Constructor for Date class
 ************************************************/
Date::Date(int yr, int mon, int dy){
  year = yr;
  month = mon;
  day = dy;
}

/************************************************
 * * Function Name: ~Date
 * * Preconditions: none
 * * Postconditions: none
 * * Destructor for Date class
 ************************************************/
Date::~Date()
{
}

/************************************************
 * * Function Name: getDay
 * * Preconditions: none
 * * Postconditions: int
 * * Returns value of Day member
 ************************************************/
int Date::getDay(){
  return day;
}
/************************************************
 * * Function Name: getMonth
 * * Preconditions: none
 * * Postconditions: int
 * * Returns value of month member
 ************************************************/
int Date::getMonth(){
  return month;
}
/************************************************
 * * Function Name: getYear
 * * Preconditions: none
 * * Postconditions: int
 * * Returns the value of the Year member 
 ************************************************/

int Date::getYear(){
  return year;
}
/************************************************
 * * Function Name: setDay
 * * Preconditions: int
 * * Postconditions: void
 * * sets the value of the day member
 ************************************************/

void Date::setDay(int x){
  day = x;
}
/************************************************
 * * Function Name: setMonth
 * * Preconditions: int
 * * Postconditions: void
 * * sets the value of the month member
 ************************************************/

void Date::setMonth(int x){
  month = x;
}
/************************************************
 * * Function Name: setYear
 * * Preconditions: int
 * * Postconditions: void
 * * sets the value of the year member
 ************************************************/

void Date::setYear(int x){
  year = x;
}
/************************************************
 * * Function Name: printDate
 * * Preconditions: ostream&
 * * Postconditions: void
 * * Prints the year month and day of the object in that order
 ************************************************/

void Date::printDate(ostream& ofs){
  ofs<< year << " " << months_list[month-1] << " " << day << " ";
}
/************************************************
 * * Function Name: compareDates
 * * Preconditions: ostream& Date&
 * * Postconditions: void
 * * Compares two Date objects and outputs the result of the comparison
 ************************************************/


void Date::compareDates(ostream& os, Date& expdate){
  if (expdate.getYear() == year){
    if (expdate.getMonth() == month){
      if (expdate.getDay() >= day)
	{
        os << "The check date ";
        expdate.printDate(os);
        os << " is the same day or after the inital date ";
	printDate(os);
	os << endl;
	}
    else{ 
      os <<"The check date ";
      expdate.printDate(os);
      os <<"is before the initial date ";
      printDate(os);
      os << endl;
    }
    }
    else if(expdate.getMonth() > month){
      os <<"The check date ";
      expdate.printDate(os);
      os << "is before the initial date ";
      printDate(os);
      os << endl;
    }
    else if(expdate.getMonth() < month){
      os << "The check date ";
      expdate.printDate(os); 
      os<<"is the same day or after the inital date ";
      printDate(os);
      os << endl;
    }
  else if(expdate.getYear() > year){
    os << "The check date ";
    expdate.printDate(os);
    os << "is before the initial date ";
    printDate(os);
    os << endl;
  }
  else if(expdate.getYear() < year){
    os << "The check date ";
    expdate.printDate(os);
    os <<"is the same day or after the inital date ";
    printDate(os);
    os << endl;
  }
  }
}




