/***************************
 * File Name: testDate.cpp
 * Author: Nathaniel Ekwueme Stephen Grisoli Aidan Gonzales Michael Deranek
 * Email: nekwueme@nd.edu mderane1@nd.edu agonza31@nd.edu sgrisoli@nd
 * Description: Main driver for "date" class. Program creates four dates--an intial expiration date to be compared to and three check dates. It outputs the result to testDateOut.txt
 * *************************/

//Main Driver for pill bottle data structure

#include <iostream>
#include <vector>
#include <vector>
#include "date.h"
#include <fstream>
using namespace std;

int main(){
  ofstream ofs;
  string outfile = "testDateOut.txt";
  ofs.open(outfile);
  Date int_date1(2019,1,23);
  Date int_date2(2019,1,25);
  Date int_date3(2019,1,18); 
  Date expdate(2019,1,23);

  int_date1.compareDates(ofs, expdate);
  int_date2.compareDates(ofs, expdate);
  int_date3.compareDates(ofs, expdate);
return 0;
}

