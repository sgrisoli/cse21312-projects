/**************************
* File name: Bottle.h
* Author: Nathaniel Ekwueme, Stephen Grisoli, Aidan Gonzales, Michael Deranek
* Email: nekwueme@nd.edu sgrisoli@nd.edu mderane1@nd.edu agonza31@nd.edu
* 
* Description: Header file for "Bottle" class
* ***************************/
#include "date.h"
using namespace std;

class Bottle{
  friend ostream& operator<< (ostream &, Bottle &);
  friend istream& operator>> (istream &, Bottle &);
  public:
    Bottle();
    Bottle(string, int, Date);
    ~Bottle();
    string getName();
    int getLot();
    Date getDate();
    void setName(string);
    void setLot(int);
    void setDate(Date);
    void removePill(int);


  private:
    string name;
    int lot;
    Date expDate;
};
