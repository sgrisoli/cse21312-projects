/***************************
* File name: date.h
* Author: Nathaniel Ekwueme, Stephen Grisoli, Aidan Gonzales, Michael Deranek
* Email: nekwueme@nd.edu sgrisoli@nd.edu mderane1@nd.edu agonza31@nd.edu
*
* Description: Header file for "date" class
* ***************************/
using namespace std;
#include <vector>
#include <string>

class Date{
  public:
    Date();
    Date(int,int,int);
    Date(const Date &);
    ~Date();
    int getDay();
    int getMonth();
    int getYear();
    void setDate(int, int, int);
    void setDay(int);
    void setMonth(int);
    void setYear(int);
    void printDate(ostream&); 
    void compareDates(ostream &, Date);

  private:
    int year, month, day;
    vector<string> months_list {"JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"};

};
