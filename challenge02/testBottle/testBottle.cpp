/********************
 * File Name: testBottle.cpp
 * Author: Nathaniel Ekwueme, Stephen Grisoli, Aidan Gonzales, Michael Deranek
 * Email: nekwueme@nd.edu sgrisoli@n.edu mderane1@nd.edu agonza31@nd.edu
 *
 * Description: Main Driver for Bottle Class
 * *****************/

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include "bottle.h"
using namespace std;
/************************************************
 * Function Name: Dispense
 * Preconditions: Bottle, Bottle &, ofstream&
 * Postconditions: void
 * Function to dispense correct number of pills or inform user that there are not enough in bottle
 * ************************************************/
void dispense(Bottle, Bottle &, ofstream&);
/************************************************
 * Function Name: createFile
 * Preconditions: ifstream&, string
 * Postconditions: void
 * Takes in a string, creates an input file stream, and checks that filestream.
 * ************************************************/
void createFile(ifstream &, string);
/************************************************
 * Function Name: compareDates
 * Preconditions: Date, (expiration)Date
 * Postconditions: bool
 * Returns true if expired and false if not expired
 * ************************************************/
bool compareDates(Date, Date);

/************************************************
 * Function Name: main
 * Preconditions: int, char*
 * Postconditions: int
 * Main function for testing a row of the Bottle class
 * ************************************************/
int main(int argc, char* argv[]){
  string filename = argv[1];
  string outfile = "outfile.txt";
  string rowname = "Lisinopril";
  ifstream ifs;
  ofstream ofs;
  Bottle b2;
  Bottle b1("Azithromycin", 45, Date(2019, 1, 23));
  string year_s, month_s, day_s, name, lot_s;
  int lot;

  createFile(ifs, filename);
  ofs.open(outfile);

  ofs << b1 << endl;
  ofs << b2 << endl;
  string c;
  getline(ifs, c, ' ');
  while(!ifs.eof()) {
    ofs << c << ": ";
    if(c == "STOCK") {
      ifs >> b2;
      ifs.ignore();
      ofs << b2;
      }
    else if(c == "INSPC") {
      getline(ifs, year_s, ' ');
      getline(ifs, month_s, ' ');
      getline(ifs, day_s);
      Date insp(stoi(year_s), stoi(month_s), stoi(day_s));
      ofs << "INSPECTION ON ";
      insp.printDate(ofs);
      ofs << endl;
      ofs << "For Bottle 1: ";
      b1.getDate().compareDates(ofs, insp);
      ofs << "For Bottle 2: ";
      b2.getDate().compareDates(ofs, insp);
    }   
    else if (c == "SCRIP") {
      getline(ifs, year_s, ' ');
      getline(ifs, month_s, ' ');
      getline(ifs, day_s, ' ');
      Date disp(stoi(year_s), stoi(month_s), stoi(day_s));
      getline(ifs, name,  ' ');
      getline(ifs, lot_s);
      lot = stoi(lot_s);
      Bottle b3(name, lot, disp);
      if (b3.getName() == "Azithromycin")
          dispense(b3, b1, ofs);
      else if (b3.getName() == "Lisinopril")
          dispense(b3, b2, ofs);
    }
    getline(ifs, c, ' ');
    ofs << endl << endl;
  }

  ifs.close();
  return 0;
}

void createFile(ifstream & fs, string nm) {
  fs.open(nm);
  if(!fs) {
    cout << "error: requested file is not accessible." << endl;
  }
}

void dispense(Bottle nw, Bottle &stk, ofstream &ofs) {
    if(stk.getName() == nw.getName()) {
        if(compareDates(stk.getDate(), nw.getDate())){
          ofs << "Bottle is expired on: ";
          stk.getDate().printDate(ofs);
          ofs << endl;
        }
        else {
          if(stk.getLot() < nw.getLot()) {
            ofs << "Presciption exceeds number of pills in bottle. Can only dispense " << stk.getLot() << " pills. " << stk.getName() << " Pills = 0. EXP: ";
            stk.getDate().printDate(ofs);
            stk.setLot(0);
          }
          else {
            ofs << "Sufficient stock in bottle. Currently have " << stk.getLot() << " " << stk.getName() << ". Dispensing " << nw.getLot() << " " << stk.getName() << " Pills = " << stk.getLot() - nw.getLot() << ". EXP: ";
            stk.getDate().printDate(ofs);
            stk.setLot(stk.getLot() - nw.getLot());
          }
        }
    }
}

bool compareDates(Date a, Date expdate){
  if (expdate.getYear() == a.getYear()){
    if (expdate.getMonth() == a.getMonth()){
      if (expdate.getDay() < a.getDay())
	{ return false; }
      else{ return true; }
    }
    else if(expdate.getMonth() < a.getMonth())
      return false;
    else if(expdate.getMonth() > a.getMonth())
      return true; }
  else if(expdate.getYear() < a.getYear())
    return false;
  else if(expdate.getYear() > a.getYear())
    return true;
}
