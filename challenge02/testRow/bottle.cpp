/***************************
* File name: Bottle.cpp
* Author: Nathaniel Ekwueme, Stephen Grisoli, Aidan Gonzales, Michael Deranek
* Email: nekwueme@nd.edu sgrisoli@nd.edu mderane1@nd.edu agonza31@nd.edu
*
* Description: Implementation file for "Bottle" class
* ***************************/

#include <iostream>
#include <string>
#include "bottle.h"
using namespace std;

/************************************************
 * Function Name: Bottle
 * Preconditions: none
 * Postconditions: Bottle
 * Default constructor for Bottle class
 * ************************************************/

Bottle::Bottle()
{
  name = "No Name";
  lot = 0;
  expDate = Date(2019, 1, 1);
}

/************************************************
 * Function Name: Bottle
 * Preconditions: none
 * Postconditions: Bottle
 * Constructor for Bottle class
 * ************************************************/

Bottle::Bottle(string nm, int lt, Date dt)
{
  setName(nm);
  setLot(lt);
  setDate(dt);
}

/************************************************
 * Function Name: ~Bottle
 * Preconditions: none
 * Postconditions: none
 * Destructor for Bottle class
 * ************************************************/
 
Bottle::~Bottle()
{ }

/************************************************
 * Function Name: getName
 * Preconditions: none
 * Postconditions: string
 * Returns value of Name member
 * ************************************************/

string Bottle::getName()
{ return name; }

/************************************************
 * Function Name: getLot
 * Preconditions: none
 * Postconditions: int
 * Returns value of Lot member
 * ************************************************/

int Bottle::getLot()
{ return lot; }

/************************************************
 * Function Name: setName
 * Preconditions: string
 * Postconditions: void
 * Sets the value of the Name member
 * ************************************************/

Date Bottle::getDate()
{ return expDate; }

/************************************************
 * Function Name: setName
 * Preconditions: string
 * Postconditions: void
 * Sets the value of the Name member
 * ************************************************/

void Bottle::setName(string nm)
{ name = nm; }

/************************************************
 * Function Name: setLot
 * Preconditions: int
 * Postconditions: void
 * Sets the value of the Lot member
 * ************************************************/

void Bottle::setLot(int lt)
{ lot = lt; }

/************************************************
 * Function Name: setLot
 * Preconditions: int
 * Postconditions: void
 * Sets the value of the Lot member
 * ************************************************/

void Bottle::setDate(Date dt)
{ expDate = dt; }

/************************************************
 * Function Name: removePill
 * Preconditions: int
 * Postconditions: void
 * Removes pills from the object
 * ************************************************/

void Bottle::removePill(int num)
{ setLot(lot - num); }

/************************************************
 * Function Name: operator <<
 * Preconditions: ostream &, Bottle &
 * Postconditions: ostream&
 * Outputs the name, lot, and date of bottle
 * ************************************************/

ostream& operator<< (ostream & os, Bottle & c) {

  os << c.getName() << ". PILLS: " << c.getLot() << ". EXP: ";
  c.expDate.printDate(os); 
  os << endl;

  return os;
}

/************************************************
 * Function Name: operator >>
 * Preconditions: istream &, Bottle &
 * Postconditions: istream&
 * Allows the user to input contents of a Bottle class object
 * ***********************************************/

istream& operator>> (istream & is, Bottle & c) {

  string nme;
  int lt, yr, mn, dy;


  is >> nme >> lt >> yr >> mn >> dy;

  c.setName(nme); 
  c.setLot(lt);
  c.expDate.setYear(yr);
  c.expDate.setMonth(mn);
  c.expDate.setDay(dy);

  return is;
}
