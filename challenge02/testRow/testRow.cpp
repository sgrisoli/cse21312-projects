/**********************
 * File name: testBottle.cpp
 * Author Nathaniel Ekwueme Stepen Grisoli Aidan Gonzales Michael Deranek
 * Email nekwueme@nd.edu sgrisoli@nd.edu mderane1@nd.edu agonza31@nd.edu
 * 
 * Description Main Driver for TestRow
 * *********************/

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <queue>
#include "bottle.h"
using namespace std;

void dispense(queue <Bottle>* , Bottle, ostream&);
void createFile(ifstream &, string);
bool compareDates(Date, Date);

int main(int argc, char* argv[]){
  string filename = argv[1];
  string outfile = "outfile.txt";
  string rowname = "Lisinopril";
  ifstream ifs;
  ofstream ofs;
  Bottle stock;
//  Bottle b("Lisinopril", 0, Date(2020, 12, 15));
  queue <Bottle> r;
//  r.push(b);
  string year_s, month_s, day_s, name, lot_s;
  int lot;

  cout << stock << endl;
//  cout << b << endl;

  createFile(ifs, filename);
  ofs.open(outfile);

  string c;
  getline(ifs, c, ' ');
  while(!ifs.eof()) {
//    ofs << c << endl
    if(!r.empty()){
      while(r.front().getLot() == 0)
        r.pop();
    }
    ofs << c << ": ";
    if(c == "STOCK") {
      ifs >> stock;
      ifs.ignore();
      ofs << "Attempting to stock new bottle: " << stock;
      if(r.empty()){
         r.push(stock);
         ofs << "Successful stock!" << endl;
         }
      else if(stock.getName() == rowname) {//r.front().getName()) {
       // if(r.front().getLot() == 0) r.pop();
        r.push(stock);
        ofs << "Successful stock!" << endl;
      }
      else ofs << "This row is not for " << stock.getName() << ". Cannot be stocked." << endl;
    }
    else if(c == "INSPC") {
   ///   ifs.ignore();
      if(!(r.empty())) {
        getline(ifs, year_s, ' ');
        getline(ifs, month_s, ' ');
        getline(ifs, day_s);
        Date insp(stoi(year_s), stoi(month_s), stoi(day_s));
        ofs << "INSPECTION ON ";
        insp.printDate(ofs);
        ofs << endl;
        if(compareDates(r.front().getDate(), insp)) {
          //r.front().getDate().printDate(ofs);
          insp.printDate(ofs);
          //r.pop();
          ofs <<"Bottle was expired and removed" << endl;
        }
        /*
        if(!(r.empty()))
          if(r.front().getLot() > 0)
        */
        else ofs << "Inspection passed! Front bottle expires on ";
	r.front().getDate().printDate(ofs);
        ofs << endl;
         //  else if(r.front().getLot() == 0) ofs << "Row is empty! No bottles to inspect." << endl;
      }
      else {
        ofs << "Row is empty! No bottles to inspect." << endl;
        getline(ifs, year_s, ' ');
        getline(ifs, month_s, ' ');
        getline(ifs, day_s);
      }
    }   
    else if (c == "SCRIP") {
      getline(ifs, year_s, ' ');
      getline(ifs, month_s, ' ');
      getline(ifs, day_s, ' ');
      Date disp(stoi(year_s), stoi(month_s), stoi(day_s));
      getline(ifs, name,  ' ');
      getline(ifs, lot_s);
      lot = stoi(lot_s);
      Bottle bot1(name, lot, disp);
      ofs << "Patient brought in a prescription: " << bot1;
      if(rowname != bot1.getName()) ofs << "This row is not for " << bot1.getName() << ". Cannot be dispensed." << endl;
      else if(r.empty()){
        ofs << "Error: no medication available to dispense." << endl;
      }
      else
        dispense(&r, bot1, ofs);
    }
    getline(ifs, c, ' ');
//    cout << c << endl;
    ofs << endl;
  }

  ifs.close();
  return 0;
}

void createFile(ifstream & fs, string nm) {
  fs.open(nm);
  if(!fs) {
    cout << "error: requested file is not accessible." << endl;
  }
}

void dispense(queue <Bottle>* row, Bottle nw, ostream& ofs) {
    if(row->front().getName() == nw.getName()) {
    //  while((*row).front().getLot() == 0)
    //    (*row).pop();
      while((*row).front().getLot() > 0 && nw.getLot() > 0) {
        //if(compareDates(nw.getDate(), (*row).front().getDate())) {
          if(compareDates((*row).front().getDate(), nw.getDate())){
            ofs << "Bottle is expired on: ";
            row->front().getDate().printDate(ofs);
            ofs << endl;
            (*row).pop();
        }
        else {
          if((*row).front().getLot() <= nw.getLot()) {
            ofs << "Front bottle must be emptied. " << (*row).front().getLot() << " pills dispensed. Moving to next bottle." << endl;
            nw.setLot(nw.getLot() - (*row).front().getLot());
            (*row).pop();
          }
          else {
            ofs << "Sufficient stock in front bottle. Currently have " << (*row).front().getLot() << " pills. Dispensing " << nw.getLot() << " pills " << endl;
            (*row).front().setLot((*row).front().getLot() - nw.getLot());
            nw.setLot(0);
          }
        }
      }
      if(nw.getLot() > 0)
        ofs << "The row is empty. " << nw.getLot() << " pills still needed." << endl;
    }
    else ofs << "Prescription cannot be filled by row." << endl;
}

bool compareDates(Date a, Date expdate){
  if (expdate.getYear() == a.getYear()){
    if (expdate.getMonth() == a.getMonth()){
      if (expdate.getDay() < a.getDay())
	{ return false; }
      else{ return true; }
    }
    else if(expdate.getMonth() < a.getMonth())
      return false;
    else if(expdate.getMonth() > a.getMonth())
      return true; }
  else if(expdate.getYear() < a.getYear())
    return false;
  else if(expdate.getYear() > a.getYear())
    return true;
}

/*
  else
    if(compareDates(nw, stk))
      cout << "Bottle is expired on: " << stk.getDate();
    else
      if(stk.getLot() >= nw.getLot()) {
        stk.setLot(stk.getLot() - nw.getLot());
        cout << "All pills successfully dispersed. Remaining # of pills: " << stk.getLot();
      }
      else {
        stk.setLot(0);
        cout << "Could not complete prescription. Insufficient # of pills. Pills needed: " << nw.getLot() - stk.getLot() << endl;
      }
    
}
*/
